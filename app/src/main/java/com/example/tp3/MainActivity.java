package com.example.tp3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listVille;
    WeatherDbHelper dbHelper;
    SimpleCursorAdapter cursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        final SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);


        dbHelper = new WeatherDbHelper(this);
        dbHelper.populate();
        cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllCities(),
                new String[]{WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.
                        COLUMN_COUNTRY},
                new int[]{android.R.id.text1, android.R.id.text2});

        listVille = (ListView) findViewById(R.id.liste_ville);

        listVille.setAdapter(cursorAdapter);

       swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                AsyncTask asyncTask = new AsyncTask() {

                    @SuppressLint("WrongThread")
                    @Override
                    protected Object doInBackground(Object[] objects) {
                        try {
                            for (int i = 0; i < listVille.getCount(); ++i) {
                                City city = dbHelper.cursorToCity((Cursor) listVille.getItemAtPosition(i));
                                URL url = WebServiceUrl.build(city.getName(), city.getCountry());
                                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                JSONResponseHandler jsonResponseHandler = new JSONResponseHandler(city);
                                jsonResponseHandler.readJsonStream(connection.getInputStream());
                                dbHelper.updateCity(city);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        super.onPostExecute(o);
                        cursorAdapter.changeCursor(dbHelper.fetchAllCities());
                        cursorAdapter.notifyDataSetChanged();
                        swipeLayout.setRefreshing(false);
                    }
                }.execute();


            }




        });


        listVille.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                Cursor cursor = (Cursor) listVille.getItemAtPosition(position);
                City city = (City) dbHelper.cursorToCity(cursor);

                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtra(City.TAG, city);
                startActivityForResult(intent, 2);
            }

        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent, 1);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        City city;
        switch (resultCode) {
            case 1:
                assert data != null;
                city = data.getParcelableExtra(City.TAG);
                dbHelper.addCity(city);
                break;
            case 2:
                assert data != null;
                city = data.getParcelableExtra(City.TAG);
                dbHelper.updateCity(city);
                break;
        }
        cursorAdapter.changeCursor(dbHelper.fetchAllCities());
        cursorAdapter.notifyDataSetChanged();
    }



}


